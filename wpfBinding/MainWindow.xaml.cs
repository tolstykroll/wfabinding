﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfBinding
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Binding binding = new Binding();
            // (1)
            //binding.ElementName = "edCity";
            //binding.Path = new("Text");
            // (2)
            binding.ElementName = nameof(edCity);
            binding.Path = new(nameof(edCity.Text));

            laCity.SetBinding(TextBlock.TextProperty, binding);
        }
    }

    public class Herro
    {
        public string Name { get; set; } = "User";
        public string Clan { get; set; }
        public int HP { get; set; } = 100;
        public int Money { get; set; } = 10;
    }
}
